json.extract! reservation, :id, :book_id, :client, :references, :librarian_id, :status, :created_at, :updated_at
json.url reservation_url(reservation, format: :json)
