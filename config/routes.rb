Rails.application.routes.draw do
  resources :reservations
  resources :books
  resources :authors
  resources :clients
  resources :librarians
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
